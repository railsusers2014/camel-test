package jc.scratch.camel.routes;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;

@Component
public class SimpleRouteBuilder extends RouteBuilder {
    @Override
    public void configure() throws Exception {
        // access via http://localhost:8080/camel/hello
        from("servlet:///hello")
                .routeId("servlet-hello")
                .transform().constant("Hello from Camel!");

        // auto-trigger every 10s
        from("timer://foo?fixedRate=true&period=10s")
                .routeId("timer")
                .log("I'm alive. :)");

        // rest endpoints
        rest("/api/say")
                // access via GET http://localhost:8080/camel/api/say/hello
                .get("/hello").route()
                    .routeId("rest-hello")
                    .to("direct:hello")
                .endRest()
                // access via GET http://localhost:8080/camel/api/say/bye
                .get("/bye").route()
                    .routeId("rest-bye")
                    // respond with "Bye world!"
                    .transform().constant("Bye world!")
                .endRest()
                // access via GET http://localhost:8080/camel/api/say/hello/{name}
                .get("/hello/{name}").route()
                    .routeId("rest-hello-named")
                    .to("bean:hello?method=hello(${header.name})")
                .endRest();

        // respond with "Hello world!"
        from("direct:hello").routeId("direct-hello").transform().constant("Hello world!");
    }
}

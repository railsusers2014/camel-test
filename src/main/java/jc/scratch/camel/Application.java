package jc.scratch.camel;

import lombok.extern.slf4j.Slf4j;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.servlet.CamelHttpTransportServlet;
import org.apache.camel.spring.SpringCamelContext;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.embedded.MultipartConfigFactory;
import org.springframework.boot.context.embedded.ServletRegistrationBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

import javax.inject.Inject;
import javax.servlet.MultipartConfigElement;
import java.util.Set;

@Slf4j
@SpringBootApplication
public class Application {
    @Value("${camel.mapping:/camel/*}")
    private String camelUrlMapping;

    @Value("${camel.servletName:CamelServlet}")
    private String camelServletName;

    @Value("${multipart.maxFileSize:10MB}")
    private String multipartMaxFileSize;

    @Value("${multipart.maxRequestSize:10MB}")
    private String multipartMaxRequestSize;

    @Inject
    private Set<RouteBuilder> routeBuilders;

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Bean
    public ServletRegistrationBean servletRegistrationBean() {
        ServletRegistrationBean registration = new ServletRegistrationBean(new CamelHttpTransportServlet(), camelUrlMapping);
        registration.setMultipartConfig(multipartConfigElement());
        registration.setName(camelServletName);
        log.info("Camel registered at {} with name {}.", camelUrlMapping, camelServletName);
        return registration;
    }

    @Bean
    public MultipartConfigElement multipartConfigElement() {
        MultipartConfigFactory factory = new MultipartConfigFactory();
        factory.setMaxFileSize(multipartMaxFileSize);
        factory.setMaxRequestSize(multipartMaxRequestSize);
        log.info("Multipart config set. maxFileSize={}, maxRequestSize={}", multipartMaxFileSize, multipartMaxRequestSize);
        return factory.createMultipartConfig();
    }

    @Bean
    public SpringCamelContext camelContext(ApplicationContext applicationContext) throws Exception {
        SpringCamelContext camelContext = new SpringCamelContext(applicationContext);
        for (RouteBuilder routeBuilder : routeBuilders) {
            log.info("Adding routes: {}", routeBuilder.getClass());
            camelContext.addRoutes(routeBuilder);
        }
        return camelContext;
    }

}

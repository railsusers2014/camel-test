package jc.scratch.camel.rest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {
    @RequestMapping(value = "/api/hello")
    public String index() {
        return "Hello from REST!";
    }
}
